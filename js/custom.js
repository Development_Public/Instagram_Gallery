$(document).ready(function() {


    var userFeed = new Instafeed({
        get: 'user',
        userId: '1116678546',
        limit: 12,
        resolution: 'standard_resolution',
        accessToken: '1116678546.182aa71.48982068c7f34b57a7d18fe6b61484fe',
        sortBy: 'most-recent',
        template: '<div class="col-lg-3 instaimg"><a href="{{image}}" title="{{caption}}" target="_blank"><img src="{{image}}" alt="{{caption}}" class="img-fluid"/></a></div>',
    });


    userFeed.run();

    
    // This will create a single gallery from all elements that have class "gallery-item"
    $('.gallery').magnificPopup({
        type: 'image',
        delegate: 'a',
        gallery: {
            enabled: true
        }
    });


});