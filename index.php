<!DOCTYPE html>
<!--/*!
 * Ronnyere Andrade - E-mail:ronnyere.a@gmail.com (https://ronnyeredeveloper.blogspot.com/)
 * Copyright 2018 Todos os Direitos Reservados (https://gitlab.com/Development_Public/Instagram_Gallery.git)
 * Linkedin (https://www.linkedin.com/in/ronny-andrade/)
 */-->
<html>
<head>
	<title>Instagram Feed Test</title>


	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/magnific.css">
	<link rel="stylesheet" href="./css/style.css">

</head>
<body>




<div class="container">

	<div id="instafeed" class="row gallery"></div>

</div>



<script src="./js/jquery.min.js"></script>
<script src="./js/instafeed.min.js"></script>
<script src="./js/magnific.min.js"></script>
<script src="./js/custom.js"></script>

</body>
</html>
